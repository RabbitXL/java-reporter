import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.mail.MessagingException;
import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class General {
    static Properties settings;
    static EmailSender es;

    public static void main(String[] args) {
        Logger log = Logger.getLogger(General.class.getName());
        FileHandler fh = null;
        try {
            fh = new FileHandler("log.txt", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        fh.setLevel(Level.ALL);
        SimpleFormatter sf = new SimpleFormatter();
        fh.setFormatter(sf);
        log.addHandler(fh);
        log.info("Старт приложения");
        log.log(Level.INFO, "Чтение настроек");
        settings = new Properties();
        try {
            settings.load(new InputStreamReader(new FileInputStream("conf.prop"), "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
            log.log(Level.SEVERE, "Ошибка при чтении файла настроек");
        }

        String attachFile = new String("Отчет.xls");
        File report = new File(attachFile);
        FileInputStream inputStream = null;
        log.info("Открываем файл отчета");
        try {
            inputStream = new FileInputStream(report);
        } catch (FileNotFoundException e) {
            log.log(Level.SEVERE, "Файл отчета не найден");
            e.printStackTrace();
        }
        HSSFWorkbook workbook = null;
        try {
            workbook = new HSSFWorkbook(inputStream);
        } catch (IOException e) {
            log.log(Level.SEVERE, "Ошибка при чтении файла отчета");
            e.printStackTrace();
        }
        HSSFSheet sheet = workbook.getSheetAt(0);
        HSSFCell cell = sheet.getRow(3).getCell(1);
        try {
            inputStream.close();
        } catch (IOException e) {
            log.log(Level.SEVERE, "Ошибка при закрытии файла отчета");
            e.printStackTrace();
        }
        log.info("Получаю текущую дату");
        LocalDate today = LocalDate.now();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        String cellString = new String("   С   8-00   " + formatter.format(today) + "   до   8-00   " + formatter.format(today.plusDays(1)));
        log.info("Старая строка " + cell.toString());
        log.info("Новая строка " + cellString);
        log.info("Записываю новую строку в отчет");
        cell.setCellValue(cellString);
        FileOutputStream out = null;
        log.info("Сохраняю файл отчета");
        try {
            out = new FileOutputStream(report);
            workbook.write(out);
            out.close();
        } catch (FileNotFoundException e) {
            log.log(Level.SEVERE, "Файл не найден");
            e.printStackTrace();

        } catch (IOException e) {
            log.log(Level.SEVERE, "Ошибка сохранения отчета");
            e.printStackTrace();
        }
        log.info("Инициализация почты");
        try {
            es = new EmailSender(settings);
        } catch (MessagingException e) {
            log.log(Level.SEVERE, "Ошибка инициализации почты");
            e.printStackTrace();
                   }
        log.info("Подготовка сообщения");
        String subject = new String("Ежесуточный отчет");
        String body = new String("");
        MyMessage msg = new MyMessage(attachFile, subject, body);
        log.info("Отпрвка сообщения");
        try {
            es.sendMultiMessage(msg);
            log.info("Сообщение отправлено. Выход.");
        } catch (MessagingException e) {
            log.log(Level.SEVERE,"Ошибка при отправке сообщения");
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            log.log(Level.SEVERE,"Ошибка кодировки");
            e.printStackTrace();
        }


    }
}
