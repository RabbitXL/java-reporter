import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

class EmailSender {
    private static final String ENCODING = "UTF-8";
    static String smtpHost;
    static String smtpPort;
    static String login;
    static String password;
    static String to;
    static String from;
    static String[] toAr;


    public EmailSender(Properties settings) throws MessagingException {
        // super();
        smtpHost = settings.getProperty("server");
        smtpPort = settings.getProperty("port");
        login = settings.getProperty("user");
        password = settings.getProperty("pass");
        to = settings.getProperty("to");
        from = settings.getProperty("from");
        toAr = to.split(",");
    }

    public static void sendMultiMessage(MyMessage m) throws MessagingException, UnsupportedEncodingException {
        Authenticator auth = new MyAuthenticator(login, password);
        Properties props = System.getProperties();
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.port", smtpPort);
        props.put("mail.smtp.auth", "true");
        props.put("mail.mime.charset", ENCODING);
        Session session = Session.getDefaultInstance(props, auth);

        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(from));
        msg.setRecipient(Message.RecipientType.TO,new InternetAddress(toAr[0]));
        if (toAr.length>1){
            for (int i=1;i<toAr.length;i++){
                msg.addRecipient(Message.RecipientType.CC, new InternetAddress(toAr[i]));
            }}
        msg.setSubject(m.getSubject());

        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(m.getContent(), "text/plain; charset=" + ENCODING + "");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        MimeBodyPart attachmentBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(m.getAttachment());
        attachmentBodyPart.setDataHandler(new DataHandler(source));
        attachmentBodyPart.setFileName(MimeUtility.encodeText(source.getName()));
        multipart.addBodyPart(attachmentBodyPart);

        msg.setContent(multipart);

        Transport.send(msg);
    }
}

class MyAuthenticator extends Authenticator {
    private final String user;
    private final String password;

    MyAuthenticator(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public PasswordAuthentication getPasswordAuthentication() {
        String user = this.user;
        String password = this.password;
        return new PasswordAuthentication(user, password);
    }

}

