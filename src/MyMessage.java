

class MyMessage {



    private String subject;
    private String content;
    private String attachment;

    MyMessage(String attachment, String subject, String content) {
        this.attachment = attachment;
        this.subject = subject;
        this.content = content;

    }


    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public String getAttachment() {
        return attachment;
    }


}
